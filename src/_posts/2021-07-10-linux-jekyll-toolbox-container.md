---
layout: post-deprecated
title: "(Linux) Jekyll Toolbox container"
description: "Run Jekyll inside a Toolbox container"
toc: true
---

***Edit Aug 8, 2021 – I have edited the article to include the most up-to-date links, as well as documenting how to install via the Docker image.***

## Introduction

[Jekyll](https://jekyllrb.com/) is a Static Site Generator (SSG) written in Ruby that can be used to generate websites with minimal efforts thanks to templates and plugins. It is easy to use and very well documented. I use Jekyll to generate this website and am mostly happy with it. However, it can be tedious to set up because RubyGems (Ruby's package manager), distribution packages, and environment variables can cause problems, and debugging can become annoying.

In my case, I had a horrible experience installing Jekyll on my host system along with RubyGems and its dependencies. I'm required to add `~/gem` to my `PATH`, and later Bundle, a dependency, would create the `~/.bundle` directory. Personally, I don't want to install language specific package managers that create random dotfiles and directories that clutter my home directory on my host system, so I try to avoid them as much as I can with containers.

## What is Toolbox?

[Toolbox](https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/) is a tool to run contained environments on Linux systems without having to manually set environment variables, permissions and more. It's built on top of [Podman](https://podman.io/), a drop-in replacement for Docker, and has [minimal requirements](https://github.com/containers/toolbox/#image-requirements) to create containers.

For this reason, I wrote my own [Dockerfile](https://gitlab.com/TheEvilSkeleton/jekyll-toolbox/-/blob/main/jekyll-toolbox.Dockerfile) for Toolbox. Toolbox is convenient and doesn't require a lot of knowledge about using containers, nor does it need a lot of flags or long commands like Podman or Docker do.

## Installation

**[LPT](https://www.dictionary.com/e/acronyms/lpt/)****: Never run random commands and scripts from the Internet!** Always check the sources before running them.

Everything is available in the [Jekyll Toolbox repository](https://gitlab.com/TheEvilSkeleton/jekyll-toolbox) on GitLab, including the Dockerfile and the installation script.

Here's what you need to do:

1. if you haven't already, install the `toolbox` package on your system using your favorite package manager. If you are using Fedora Silverblue, then it is already pre-installed.
2. run the [installation script](https://gitlab.com/TheEvilSkeleton/jekyll-toolbox/-/raw/main/install.sh) manually, or install the [Docker image](https://gitlab.com/TheEvilSkeleton/jekyll-toolbox/container_registry) by running: 
```
toolbox create -i registry.gitlab.com/theevilskeleton/jekyll-toolbox
```
Alternatively, you can run the following script to avoid manually downloading the shell script to your system:<br /><br />
On a POSIX compliant shell (bash or zsh), run:
```terminal
bash <(curl -s https://gitlab.com/TheEvilSkeleton/jekyll-toolbox/-/raw/main/install.sh)
```
On fish, run:
```terminal
bash (curl -s https://gitlab.com/TheEvilSkeleton/jekyll-toolbox/-/raw/main/install.sh | psub)
```

The shell script will ask you whether you want to install using the Docker image or build it, it's up to you. This will create a new Toolbox container with the name `jekyll-toolbox`, that you will be able to use to generate websites.

### Optional: aliases

In the same shell script, I added support for adding aliases after user confirmation, so you don't have to manually alias each command or run the full command to execute it. It only supports bash, zsh, and fish. You can deny it if you want. If you use the image, then you will have to manually set the alias.

The script automatically checks which shell you are using and asks where you want to output the aliases.

If you want to manually add the aliases to your shell configuration file, add the following:

On a POSIX compliant shell (bash or zsh):
```terminal
alias bundle="toolbox run -c jekyll-toolbox bundle"
alias jekyll="toolbox run -c jekyll-toolbox jekyll"
alias gem="toolbox run -c jekyll-toolbox gem"
```
On fish:
```terminal
alias bundle "toolbox run -c jekyll-toolbox bundle"
alias jekyll "toolbox run -c jekyll-toolbox jekyll"
alias gem "toolbox run -c jekyll-toolbox gem"
```

## Usage

Unaliased commands are `toolbox run -c jekyll-toolbox COMMAND`, for example `toolbox run -c jekyll-toolbox bundle exec jekyll serve`.

If you followed the optional step, then you can use the `bundle`, `jekyll` and `gem` commands because they are aliased, for example `bundle exec jekyll serve`.

## Testing

Test it on a website built on Jekyll, like this website. Clone the website's repository and run the following commands to install the dependencies and start the webserver:

```terminal
git clone https://gitlab.com/TheEvilSkeleton/theevilskeleton.gitlab.io.git && cd theevilskeleton.gitlab.io
bundle install
bundle exec jekyll serve
```

### LoadError

The only issue I got so far is `cannot load such file -- $NAME (LoadError)`, like this one:

```terminal
❯ bundle exec jekyll serve
Configuration file: /var/home/TheEvilSkeleton/Projects/theevilskeleton.gitlab.io/_config.yml
            Source: /var/home/TheEvilSkeleton/Projects/theevilskeleton.gitlab.io
       Destination: /var/home/TheEvilSkeleton/Projects/theevilskeleton.gitlab.io/_site
 Incremental build: disabled. Enable with --incremental
      Generating... 
       Jekyll Feed: Generating feed for posts
                    done in 0.363 seconds.
 Auto-regeneration: enabled for '/var/home/TheEvilSkeleton/Projects/theevilskeleton.gitlab.io'
                    ------------------------------------------------
      Jekyll 4.2.0   Please append `--trace` to the `serve` command 
                     for any additional information or backtrace. 
                    ------------------------------------------------
/usr/local/bundle/gems/jekyll-4.2.0/lib/jekyll/commands/serve/servlet.rb:3:in `require': cannot load such file -- webrick (LoadError)
[...]
```

This can be easily fixed by executing `bundle add $NAME` in the root of the website's source code, where `$NAME` is the name that will be the output in case of error. In this example you see `webrick`, so the solution is executing `bundle add webrick` in the root of the website (if the command is aliased).

This will update your `Gemfile` and your `Gemfile.lock` files to include `$NAME` in them.

## Removing container

Removing the container is as easy as `toolbox rm -f jekyll-toolbox`.

---

Enjoy the Toolbox container!
