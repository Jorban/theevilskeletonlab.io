---
layout: post
title: "Some people think that the problems plaguing Snap also apply to Flatpak, but this is untrue"
description: "This post taps into three main arguments that users make when they are assuming that Flatpak and Snap suffer the same issues, or the problems are equally bad."
toc: true
---

## Introduction

[Flatpak](https://flatpak.org/) and [Snap](https://snapcraft.io/about) are universal package managers that provide sandboxed and containerized environments for Linux. Although they have many similarities, they have more differences. Some people think that the problems plaguing Snap also apply to Flatpak, but this is untrue. I have seen many users unintentionally enumerating Snap's problems, which I believe needs to be addressed.

This post taps into three main arguments that users make when they are assuming that Flatpak and Snap suffer the same issues, or the problems are equally bad. Do keep in mind that the quotes below are pseudoquotes, meaning that they are not the exact wording from those users, but from what I have interpreted when discussing with them.

## High disk space

> Flatpak applications and snaps take a lot more space because they install more packages and/or don't have deduplication.

Flatpak definitely takes more disk space than native packages. However, the disk space is not as large as users make it out to be. Sadly, many people see the problem of Snap's high disk space and assume that Flatpak needs as much as well. Snap uses a lot more space than Flatpak and any other package managers because Snap does not implement deduplication, so data is duplicated. Flatpak, in the other hand, does deduplication with runtimes and baseapps, which are basically the core packages, e.g. `org.gnome.Platform` for GTK applications. Instead of duplicating data for runtimes or baseapps when using multiple applications that use the same runtime or baseapp, it only takes once. Essentially, the more applications you install, the more space-efficient Flatpak becomes.

Snaps compress most, if not all data using [LZO](https://snapcraft.io/blog/why-lzo-was-chosen-as-the-new-compression-method), so they take up slightly less space than if they were uncompressed. Despite the compression, snaps still take a lot more space for applications than Flatpak applications due to the lack of deduplication. On top of that, LZO wasn't designed to be space efficient, only performant.

So, Flatpak does not take as much space as Snap, because it deduplicates core libraries, which can save hundreds of megabytes, or even gigabytes of space depending on the amount of applications installed.

The following paragraphs are off-topic and therefore hidden, but I decided to include them because they're helpful for those that want to make Flatpak more space efficient. You can click on "Click to expand" to expand it. If you're uninterested, you can skip them and read the rest of the article.

<details>
  <summary>Click to expand</summary>
    <p>Flatpak doesn't compress data yet, and I don't think it will anytime soon because it doesn't need to, but users who want to make Flatpak more space-efficient can use btrfs compression or other filesystem-level compression.</p>
    <p>At the time of writing this article, I have 146 Flatpak packages installed in my user directory, 63 of which are applications. I am using btrfs with zstd:1 (level 1) compression. Here you can see how much disk space a system with this many Flatpak applications would need:</p>

    <div class="language-terminal highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="gp">TheEvilSkeleton@TheMainLaptop ~&gt;</span><span class="w"> </span>flatpak list --all | wc -l
146
TheEvilSkeleton@TheMainLaptop ~&gt; flatpak list --app --user | wc -l
63
TheEvilSkeleton@TheMainLaptop ~&gt; sudo compsize -x ~/.local/share/flatpak/
Processed 3548950 files, 199313 regular extents (3128593 refs), 708958 inline.
Type       Perc     Disk Usage   Uncompressed Referenced
TOTAL       74%       19G          26G         101G
none       100%       16G          16G          60G
zstd        34%      3.5G          10G          41G
  </code></pre></div></div>
    <p>Here you can see that <code class="language-plaintext highlighter-rouge">~/.local/share/flatpak</code>, where Flatpak applications are installed in the user directory, consume about 26G uncompressed, but only 19G when compressed with zstd:1. This is a huge space savings, as it only takes a little more than 70% when it's compressed using zstd:1. If you want to save space when using Flatpak, you can use a filesystem that compresses data.</p>
</details>

## Slow launches

> Flatpak and snaps take a lot longer to launch apps because they have to mount many directories, load more files, set environments, etc.

Theoretical problems don't always translate into practice. Theoretically, Flatpak slows down startup times: bind mounting directories, setting environments, loading a tiny fraction more of data, etc. add a couple of milliseconds when launching applications. The performance difference between launching a native application and its Flatpak alternative is imperceptible to the eye, except in edge cases like bugs, doing a side by side comparison in slow motion, or when using decade years old hardware. Unlike Snap, Flatpak does not compress applications, and therefore does not spend time decompressing data. Filesystem compression like btrfs compression won't noticeably hinder performance either, as it is really efficient, optimized and well tested by experienced people, except in edge cases.

As mentioned several times, Snap compresses its data, which noticeably slows down startup time. The differences in startup time are very minor to imperceptible for small programs like CLI programs, since those applications inherently take very little space, and therefore take very little time to decompress. However, because GUI applications are much larger, take much more space than CLI programs, and are generally more complex, their startup time becomes noticeably slower because Snap must decompress a lot more data.

Briefly put, theoretical problems do not always matter in practice. Flatpak works well on most modern hardware because modern hardware were made to handle complex tasks simultaneously. The speed "deterioration" when starting applications hardly matter to the end user because it's small enough to not be a problem.

## Corporate driven

> Flatpak and Snap are owned and endorsed by corporations. Red Hat owning the former and Canonical owning the latter.

For clarification purposes, Red Hat holds copyright over most of the source code. However, external contributors and organizations hold copyright over their code contributions, as there is no Contributors License Agreement (CLA) or Developer Certificate of Origin (DCO). Flatpak is a cross-organizational-distribution effort and developed as part of the freedesktop.org project, whereas Snap is mainly a Canonical effort. Additionally, the "Flatpak®" trademark is owned by [GNOME](https://foundation.gnome.org/legal-and-trademarks/#:~:text=The%20word%20GTK%C2%AE-,the%20word%20flatpak%C2%AE,-The%20word%20Flathub%C2%AE).

Flatpak is endorsed by many organizations, this includes [GNOME](https://developer.gnome.org/documentation/introduction/flatpak.html), [Fedora](https://fedoraproject.org/wiki/Flatpak#Fedora_flatpaks), [EndlessOS](https://flatpak.org/setup/Endless%20OS/), [KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak), [elementaryOS](https://blog.elementary.io/elementary-appcenter-flatpak/), [openSUSE](https://openbuildservice.org/2021/02/18/introducing-flatpak-builds/), [Red Hat](https://developers.redhat.com/blog/2020/08/12/introducing-the-red-hat-flatpak-runtime-for-desktop-containers), Collabora and others. And obviously it is endorsed by a large portion of the community too.

Just like systemd and Fedora, Flatpak is politically independent from Red Hat, meaning the maintainers of the project make decisions of their own behalf and not on behalf of Red Hat, while accepting ideas from many people and organizations, therefore it is community-driven by definition.

## Conclusion

In conclusion, many of Snap's problems do not apply to Flatpak. Many users imply that Flatpak suffers from X problems since Snap suffers from X problems, because they are both universal standards that provide sandboxed and containerized environments.

It is important to understand that while Flatpak and Snap have many similarities, they have many differences. Flatpak was developed for the desktop, whereas Snap was developed for the Internet of Things (IoT), but was later adapted to the desktop. Flatpak works well in many ways and offers more than traditional package managers on the desktop. It takes advantage of modern hardware, just like other complex software like btrfs and systemd, which have theoretical problems just like Flatpak, but fortunately do not appear in practice.

---

- Initial writeup — (2021-02-11)
- Edit 1: Remove AppImage reference in themes section
- Edit 2: Remove themes paragraph because Snap allows theming; replace mentions of "shared libraries" with "deduplication" because it's technically incorrect; improve sentences in corporate driven paragraph; credit to mwleeds and vidal72 at `#flatpak:matrix.org`
