---
layout: post-deprecated
title: "Mirror GitLab repositories"
description: "A guide to mirror GitLab repositories to other software development platforms"
toc: true
---

## Introduction

GitLab is an [open core](https://about.gitlab.com/blog/2016/07/20/gitlab-is-open-core-github-is-closed-source) Git-repository hosting service, just like GitHub. It is used by big organizations and institutions like [GNOME](https://gitlab.gnome.org/), [KDE](https://invent.kde.org/), [Standford](https://code.stanford.edu/explore), and many more. GitLab comes in two different distributions: Community Edition (CE) and Entreprise Edition (EE). The former is free and open source, and the latter is built on top of GitLab CE, but is source-available and proprietary, therefore following the "open core" model. GitLab EE comes with more features than GitLab CE, as well as corporate support; it is free in price, just like GitLab CE, but you can upgrade to a paid tier to gain access to even more features. You can read more about GitLab CE and EE in the [official website](https://about.gitlab.com/install/ce-or-ee/).

Needless to say, GitLab has a neat feature called "push mirror," which lets you push repositories to other Git hosting services, such as GitHub, Gitea, etc. At the moment, I mirror some of my repositories to my [Codeberg](https://codeberg.org/TheEvilSkeleton) account, a Gitea instance, from [GitLab](https://gitlab.com/TheEvilSkeleton). GitLab EE takes the mirroring a step further by allowing you to pull from repositories outside of the instance, called "pull mirrors".

I'm going to go through the simplest methods of mirroring from and to repositories using GitLab.

## Setting up push mirror

There are three different methods to setup push mirrors: via HTTP or HTTPS protocols, via SSH protocol, and via Git protocol. In this guide, I will only go over the first and the second methods.

### Method 1: via HTTP or HTTPS

The easiest way to push repositories is via HTTP or HTTPS. However, if you use GitHub, I recommend using the [second method, via SSH](#method-2-via-ssh) and continuing from there, because GitHub recently [deprecated](https://docs.github.com/en/get-started/getting-started-with-git/about-remote-repositories#:~:text=When%20you%20git,Git%20is%20deprecated) password-based authentication (HTTP/HTTPS).

Alright, so here's what you need to do:

1. Go to your repository.
2. Go to `Settings` > `Repository`.
3. Expand `Mirroring repositories`.
4. Set `Mirror direction` to `Push`.
5. Copy and paste the link of the repository you want to mirror to inside `Git repository URL`.
6. Type the password of your account of the Git-repository hosting service you are using; if you are pushing to GitHub, use your GitHub password.
7. Press on the blue `Mirror repository` button. The page will refresh, and you will see an entry inside `Mirrored repositories`.

To confirm that you set up the push mirror correctly, press on the refresh button in the entry and check that it pushes without any error. If there is no error, double check that you set it up correctly by visiting the mirror's link and checking whether it has fetched the latest commits or not.

**Note**: you might need to check the `Mirror only protected branches` box when setting up the push mirror to get it working as intended.

### Method 2: via SSH

The second and more secure method is to push via SSH. The only downsides to this are that it can be annoying and time consuming to setup and it can potentially become hard to manage the SSH keys if you have to set a lot of push mirrors.

With that out of the way, here's what you need to do:

1. Go to your repository.
2. Go to `Settings` > `Repository`.
3. Expand `Mirroring repositories`.
4. Set `Mirror direction` to `Push`.
5. Enter `ssh://git@<host>/<group>/<repo>.git`, where `<host>/<group>/<repo>.git` is the link of your repository, for example `ssh://git@gitlab.com/TheEvilSkeleton/theevilskeleton.gitlab.io.git`. Unfortunately, SCP style (`git@example.com:group/repo.git`) isn't supported.
6. Enter the password of your account of the Git-repository hosting service you are using; if you are pushing to a GitHub repository, use your GitHub password.
7. Press on the blue `Mirror repository` button. The page will refresh, and you will see an entry inside `Mirrored repositories`.
8. Press on the clipboard button in the entry inside `Mirrored repositories`, this will copy the SSH public key.
9. Go to the Git-repository hosting service you are pushing. If you are pushing to GitHub, go to GitHub's website.
10. Go to the SSH settings of that same service. If you are using GitHub, press on your profile picture at the top right, then go to `Settings` > `SSH and GPG keys`.
11. Press on `New SSH key`.
12. Name the title to what you desire. I recommend giving it the same title as your repository.
13. Paste the key you have copied in step 8 inside the Key box.

Just like the first method, to confirm that you have set up the push mirror correctly, press on the refresh button in the entry and check that it pushes without any error. If there is no error, double check that you set it up correctly by visiting the mirror's link and checking whether it has fetched the latest commits or not.

**Note**: you might need to check the `Mirror only protected branches` box when setting up the push mirror to get it working as intended.

## Setting up pull mirror (GitLab EE only)

Unfortunately, pulling from other repositories is a GitLab Entreprise Edition only feature. The only instance that I know of with EE installed is the one managed by GitLab Inc.: [gitlab.com](https://gitlab.com).

Every hour, GitLab will attempt to pull from the wanted repository automatically and push it to your GitLab repository.

There are two methods to set up pull mirrors: importing a project, and manually setting it up from an existing project.

### Method 1: importing project

The first method is by simply importing the project because it does everything for you. As of time of writing this post, here is what you need to do:

1. Press on `≡ Menu` at the top left of GitLab.
2. Go to `Projects`.
3. Press on `Create new project`.
4. Press on `Import project`.
5. Press on `Repo by URL`.
6. Copy and paste the link of the repository you want to mirror from inside `Git repository URL`.
7. Check `Mirror repository` box.
8. Fill out the rest and then press on `Create project`.

It's that easy. To confirm that you have set up the pull mirror correctly, go to `Settings` > `Repository`, expand `Mirroring repositories` and check if your repository is mirrored.

### Method 2: from existing project

If you have an already imported repository, then here is what you need to do:

1. Go to your repository.
2. Go to `Settings` > `Repository`.
3. Expand `Mirroring repositories`.
4. Copy and paste the link of the repository you want to mirror from inside `Git repository URL`.
5. Make sure that `Mirror direction` is set to `Pull`.
6. Press on the blue `Mirror repository` button. The page will refresh, and you will see an entry inside `Mirrored repositories`.

To confirm that you have set up the mirror correctly, press on the refresh button in the entry and check that it pulls without any error.

## Conclusion

GitLab mirrors are really convenient since you don't have to use the command-line to either push or pull. GitLab Community Edition allows you to setup push mirrors, and GitLab Entreprise Edition additionally lets you setup pull mirrors.

Needless to say, there are many other ways to mirror repositories using GitLab. The ones mentioned above are the simplest and most intuitive methods. If you want to read the technical methods, see the [official documentation](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html). Enjoy mirroring!
