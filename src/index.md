---
title: TheEvilSkeleton
description: "Welcome to my personal website! I talk about myself, computers, GNU/Linux ecosystems, open-source, and other topics related to software."
layout: home
---

<h2><code>[Tesk ~]$ cat /etc/motd <span class="cursor">█</span></code></h2>

## Welcome to my personal website!

I talk about myself, computers, GNU/Linux ecosystems, open-source, and other topics related to software. You can look through my writeups and information about myself in the header at the top of the page.

## Contact

You can contact me in the following platforms:

- Email: [theevilskeleton@riseup.net](mailto:TheEvilSkeleton <theevilskeleton@riseup.net>)
- \[matrix\]: [@theevilskeleton:fedora.im](https://matrix.to/#/@theevilskeleton:fedora.im)
- Mastodon: <a rel="me" href="https://fosstodon.org/@TheEvilSkeleton">@TheEvilSkeleton@fosstodon.org</a>

## Where to find me

You can ~~stalk~~ find me in the following platforms:
- Version control websites:
    - [Codeberg](https://codeberg.org/TheEvilSkeleton/)
    - [GitHub](https://github.com/TheEvilSkeleton)
    - [GitLab](https://gitlab.com/TheEvilSkeleton)
- [Flathub Discourse](https://discourse.flathub.org/u/theevilskeleton/)
- <a rel="me" href="https://fosstodon.org/@TheEvilSkeleton">Mastodon</a>

---

Source code available in [GitLab](https://gitlab.com/TheEvilSkeleton/theevilskeleton.gitlab.io), mirrored on [Codeberg](https://codeberg.org/TheEvilSkeleton/pages), licensed under [AGPLv3](https://gitlab.com/TheEvilSkeleton/theevilskeleton.gitlab.io/-/blob/main/LICENSE).<br>
Posts under CC BY-SA 4.0 unless otherwise noted.

Issue tracker available in [GitLab](https://gitlab.com/TheEvilSkeleton/theevilskeleton.gitlab.io/-/issues).

Profile picture available in [Danbooru](https://danbooru.donmai.us/posts/3908043?q=google_chrome_%28merryweather%29), drawn by [Merryweather Comics](https://merryweather-comics.tumblr.com).
